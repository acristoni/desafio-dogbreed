import api from './api';

const getToken = (email: string): Promise<string> => {
  const header = {
    'Content-Type': 'application/json',
  };

  if (!email) {
    throw console.log('e-mail não digitado');
  }

  return api
      .post(
          '/register',
          {
            email: email,
          },
          {
            headers: header,
          },
      )
      .then((response) => response.data.user.token);
};

export default getToken;
