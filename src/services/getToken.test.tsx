import getToken from './getToken';

const emailMock = 'test@test.com';

describe('Get token JTW service', () => {
  test('Service function is returning JWTtoken', () => {
    jest.setTimeout(30000);

    return getToken(emailMock).then((response) => {
      expect(response).toBeDefined();
    });
  });
});
