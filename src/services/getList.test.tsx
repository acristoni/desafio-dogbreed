import getList from './getList';

const tokenTest =
  // eslint-disable-next-line max-len
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIyYzJkYzExZC0zODlkLTRiMjQtYjk2Ny05MTE5MTJiOTA0NTUiLCJzdWIiOiI2MmVhOTVjNzdmNzA0OTBlZDJiOGVjYzMiLCJpYXQiOjE2NTk1NDA5MzUsImV4cCI6MTY2MDgzNjkzNX0.PpqDM-wjta9GeMIjWnWs1asrVfG6kBrEyc2lIpLiOF0';
const breedDefaultTest = '';
const breedSpecifyTest = '?breed=pug';

describe('Get access to dog pictures db', () => {
  test('Service function is returning dog list default', () => {
    jest.setTimeout(30000);

    return getList(tokenTest, breedDefaultTest).then((response) => {
      expect(response).toBeDefined();
    });
  });

  test('Service function is returning dog list specify', () => {
    jest.setTimeout(30000);

    return getList(tokenTest, breedSpecifyTest).then((response) => {
      expect(response).toBeDefined();
    });
  });
});
