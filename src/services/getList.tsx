import api from './api';

const getList = (token: string, breed: string): Promise<Array<string>> => {
  const header = {
    'Content-Type': 'application/json',
    'Authorization': token,
  };

  if (!token) {
    throw console.log('usuário sem cadastro');
  }

  return api
      .get(`/list${breed}`, {
        headers: header,
      })
      .then((response) => response.data.list);
};

export default getList;
