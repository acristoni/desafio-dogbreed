/* eslint-disable require-jsdoc */
/* This rule was deprecated in ESLint v5.10.0. */
import React from 'react';
import {useEffect, useState} from 'react';
import getToken from './services/getToken';
import getList from './services/getList';
import Login from './components/login/Login';
import Dogs from './components/dogs/Dogs';
import Menu from './components/menu/Menu';
import * as S from './styled';

function App() {
  const [tokenState, setTokenState] = useState('');
  const [arrayPicturesState, setArrayPicturesState] = useState(['']);
  const [breedState, setBreedState] = useState('');

  const sendEmail = async (email: string) => {
    const token = await getToken(email);
    setTokenState(token);
  };

  const getBreed = (breed: string) => {
    setBreedState(breed);
  };

  useEffect(() => {
    if (tokenState.length > 0) {
      const getPictures = async (token: string, breed: string) => {
        const arrayPictures = await getList(token, breed);
        setArrayPicturesState(arrayPictures);
      };
      getPictures(tokenState, breedState);
    }
  }, [tokenState, breedState]);

  return (
    <div>
      <Login sendEmail={sendEmail} />
      <Menu getBreed={getBreed} />
      {tokenState.length > 0 ? (
        <Dogs arrayPicturesState={arrayPicturesState} />
      ) : (
        <S.Title>Preencha um e-mail válido para ter acesso a galeria!</S.Title>
      )}
    </div>
  );
}

export default App;
