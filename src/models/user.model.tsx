type User = {
  _id: string;
  email: string;
  token: string;
  createdAt: string;
  updatedAt: string;
  __v?: number;
};

export default User;
