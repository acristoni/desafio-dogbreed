import {User} from '../models/user.model';

declare module 'axios' {
    // TYPESCRIPT USE!
    // eslint-disable-next-line no-unused-vars
    interface AxiosResponse {
        user?: User
    }
}
