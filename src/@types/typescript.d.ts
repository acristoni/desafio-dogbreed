import {Value} from '../models/value.model';

declare module 'typescript' {
    // TYPESCRIPT USE!
    // eslint-disable-next-line no-unused-vars
    interface EventTarget {
        value?: Value
    }
}
