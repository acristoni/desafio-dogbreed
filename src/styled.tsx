import styled from 'styled-components';

export const Title = styled.h1`
  font-size: 36px;
  font-family: cursive;
  text-align: center;
  margin-top: 20px;
`;
