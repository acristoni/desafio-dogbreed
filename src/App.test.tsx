import React from 'react';
import App from './App';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';

describe('App Component', () => {
  test('render page with email, button form and buttons dog breed', () => {
    render(<App />);

    const inputEmailEl = screen.getByPlaceholderText('Digite seu e-mail!');
    const buttonSubmitEl = screen.getByDisplayValue('Enviar');
    const buttonChihuahua = screen.getByText('Chihuahua');
    const buttonHusky = screen.getByText('Husky');
    const buttonPug = screen.getByText('Pug');
    const buttonLabrador = screen.getByText('Labrador');

    expect(inputEmailEl).toBeInTheDocument();
    expect(buttonSubmitEl).toBeInTheDocument();
    expect(buttonChihuahua).toBeInTheDocument();
    expect(buttonHusky).toBeInTheDocument();
    expect(buttonPug).toBeInTheDocument();
    expect(buttonLabrador).toBeInTheDocument();
  });
});
