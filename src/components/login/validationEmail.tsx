/* eslint-disable require-jsdoc */
/* This rule was deprecated in ESLint v5.10.0. */
function validationEmail(email: string) {
  let validation = false;

  for (let i: number = 0; i < email.length; i++) {
    const char = email[i];

    if (char === '@') {
      const [usuario, dominio] = email.split('@');

      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search(' ') === -1 &&
        dominio.search(' ') === -1 &&
        dominio.search('.') !== -1 &&
        dominio.indexOf('.') >= 1 &&
        dominio.lastIndexOf('.') < dominio.length - 1
      ) {
        return (validation = true);
      }
      validation = false;
      return validation;
    }
  }
}

export default validationEmail;
