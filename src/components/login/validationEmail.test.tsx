import validationEmail from './validationEmail';

describe('Testing validationEmail function', () => {
  test('real e-mail', () => {
    expect(validationEmail('test@test.com')).toBeTruthy();
  });

  test('fake e-mail, no user', () => {
    expect(validationEmail('@test.com')).toBeFalsy();
  });

  test('fake e-mail, no dominio', () => {
    expect(validationEmail('test@')).toBeFalsy();
  });

  test('fake e-mail, space inside name', () => {
    expect(validationEmail('te st@test.com')).toBeFalsy();
  });

  test('fake e-mail, space inside dominio', () => {
    expect(validationEmail('test@te st.com')).toBeFalsy();
  });

  test('fake e-mail, dominio without dot', () => {
    expect(validationEmail('test@testcom')).toBeFalsy();
  });

  test('fake e-mail, dominio without content before dot', () => {
    expect(validationEmail('test@.com')).toBeFalsy();
  });

  test('fake e-mail, dominio without content after dot', () => {
    expect(validationEmail('test@test.')).toBeFalsy();
  });

  test('fake e-mail, dominio without @', () => {
    expect(validationEmail('testtest.com')).toBeFalsy();
  });
});
