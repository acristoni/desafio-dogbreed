import styled from 'styled-components';

export const Form = styled.form`
  margin: 20px auto;
  display: flex;
  flex-direction: column;
  width: 50vw;
  align-items: center;
`;

export const EmailInput = styled.input`
  width: 100%;
  height: 30px;
  font-size: large;
  font-family: cursive;
  font-weight: bold;
  border: 5px solid black;
  border-radius: 15px;
`;

export const errorMessage = styled.span`
  margin-top: 10px;
  color: red;
  font-family: cursive;
  font-weight: bold;
  font-size: large;
`;

export const ButtonInput = styled.input`
  margin-top: 10px;
  width: 50%;
  font-size: large;
  font-family: cursive;
  font-weight: bold;
  border: 5px solid black;
  border-radius: 15px;
  background: rgb(2, 0, 36);
  background: linear-gradient(
    90deg,
    rgba(2, 0, 36, 1) 0%,
    rgba(9, 9, 121, 1) 35%,
    rgba(0, 212, 255, 1) 100%
  );
  color: white;
  text-shadow: 0 0 3px blue;
  box-shadow: 0 0 3px blue;

  :hover {
    background: rgb(63, 94, 251);
    background: radial-gradient(
      circle,
      rgba(63, 94, 251, 1) 0%,
      rgba(252, 70, 107, 1) 100%
    );
    color: white;
    text-shadow: 0 0 3px red;
    box-shadow: 0 0 3px red;
    cursor: pointer;
  }
`;
