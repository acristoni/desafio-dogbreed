import React from 'react';
import {useForm, SubmitHandler} from 'react-hook-form';
import validationEmail from './validationEmail';
import {useState} from 'react';
import * as S from './styled';

type Inputs = {
  email: string;
};

interface Props {
  sendEmail: (email: string) => {},
}

const Login: React.FC<Props> = ({sendEmail = (email: string) => {}}) => {
  const [valid, setValid] = useState(false);

  const {register, handleSubmit} = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    if (validationEmail(data.email)) {
      sendEmail(data.email);
      setValid(false);
    } else {
      setValid(true);
    }
  };
  return (
    <S.Form onSubmit={handleSubmit(onSubmit)}>
      <S.EmailInput
        {...register('email', {required: true})}
        placeholder="Digite seu e-mail!"
      />
      {valid && (
        <S.errorMessage id="mensageError">
          Esse campo deve ser preenchido com um e-mail válido!
        </S.errorMessage>
      )}
      <S.ButtonInput type="submit" value="Enviar" />
    </S.Form>
  );
};

export default Login;
