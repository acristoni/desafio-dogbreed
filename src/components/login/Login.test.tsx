import React from 'react';
import Login from './Login';
import {render, screen} from '@testing-library/react';
import '@testing-library/jest-dom';

const callback = jest.fn;
describe('test Login component', () => {
  test('input type e-mail is rendering', () => {
    render(<Login sendEmail={callback} />);
    const emailInputEl = screen.getByPlaceholderText('Digite seu e-mail!');
    expect(emailInputEl).toBeInTheDocument();
  });

  test('input type submit, button, is rendering', () => {
    render(<Login sendEmail={callback} />);
    const buttonEl = screen.getByDisplayValue('Enviar');
    expect(buttonEl).toBeInTheDocument();
  });
});
