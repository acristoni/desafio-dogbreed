import styled from 'styled-components';

export const NavBar = styled.nav`
  margin-top: 20px;
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;

  button {
    width: 200px;
    height: 50px;
    font-size: x-large;
    font-family: cursive;
    font-weight: bold;
    border: 5px solid black;
    border-radius: 15px;
    background: rgb(2, 0, 36);
    background: linear-gradient(
      90deg,
      rgba(2, 0, 36, 1) 0%,
      rgba(9, 9, 121, 1) 35%,
      rgba(0, 212, 255, 1) 100%
    );
    color: white;
    text-shadow: 0 0 3px blue;
    box-shadow: 0 0 3px blue;
  }

  button:hover {
    background: rgb(63, 94, 251);
    background: radial-gradient(
      circle,
      rgba(63, 94, 251, 1) 0%,
      rgba(252, 70, 107, 1) 100%
    );
    color: white;
    text-shadow: 0 0 3px red;
    box-shadow: 0 0 3px red;
    cursor: pointer;
  }
`;
