import React from 'react';
import * as S from './styled';

interface Props {
  getBreed: (breed: string) => void
}

const Menu: React.FC<Props> = ({getBreed = (breed: string) => {}}) => {
  return (
    <S.NavBar>
      <button onClick={() => getBreed('?breed=chihuahua')}>Chihuahua</button>
      <button onClick={() => getBreed('?breed=husky')}>Husky</button>
      <button onClick={() => getBreed('?breed=pug')}>Pug</button>
      <button onClick={() => getBreed('?breed=labrador')}>Labrador</button>
    </S.NavBar>
  );
};

export default Menu;
