import React from 'react';
import Menu from './Menu';
import {render, screen, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom';

const callback = jest.fn();

describe('test navbad dog breed', () => {
  test('buttons are rendering', () => {
    render(<Menu getBreed={callback} />);

    const buttonChihuahua = screen.getByText('Chihuahua');
    const buttonHusky = screen.getByText('Husky');
    const buttonPug = screen.getByText('Pug');
    const buttonLabrador = screen.getByText('Labrador');

    expect(buttonChihuahua).toBeInTheDocument();
    expect(buttonHusky).toBeInTheDocument();
    expect(buttonPug).toBeInTheDocument();
    expect(buttonLabrador).toBeInTheDocument();
  });

  test('button Chihuahua calls function when pressed', () => {
    const callback = jest.fn();
    render(<Menu getBreed={callback} />);
    const buttonChihuahua = screen.getByText('Chihuahua');
    fireEvent.click(buttonChihuahua);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('button Husky calls callback function when pressed', () => {
    const callback = jest.fn();
    render(<Menu getBreed={callback} />);
    const buttonHusky = screen.getByText('Husky');
    fireEvent.click(buttonHusky);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('button Pug calls callback function when pressed', () => {
    const callback = jest.fn();
    render(<Menu getBreed={callback} />);
    const buttonPug = screen.getByText('Pug');
    fireEvent.click(buttonPug);
    expect(callback).toHaveBeenCalledTimes(1);
  });

  test('button Labrador calls callback function when pressed', () => {
    const callback = jest.fn();
    render(<Menu getBreed={callback} />);
    const buttonLabrador = screen.getByText('Labrador');
    fireEvent.click(buttonLabrador);
    expect(callback).toHaveBeenCalledTimes(1);
  });
});
