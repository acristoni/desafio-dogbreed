import React from 'react';
import Dogs from './Dogs';
import {render, screen} from '@testing-library/react';

const arrayPictureMock = [''];

describe('Dog Grid', () => {
  test('Component is rendering imgs', () => {
    render(<Dogs arrayPicturesState={arrayPictureMock} />);
    const ImgEl = screen.getByAltText('Dog');
    expect(ImgEl).toBeDefined();
  });
});
