import React from 'react';
import * as S from './styled';
import Overlay from './modal/Modal';
import {useState} from 'react';

interface Props {
  arrayPicturesState: string[];
}

const Dogs: React.FC<Props> = ({arrayPicturesState}) => {
  const [urlOverlay, setUrlOverlay] = useState('');
  const [modalIsOpen, setIsOpen] = useState(false);

  const getOverlay = (url: string) => {
    setIsOpen(true);
    setUrlOverlay(url);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <S.Grid>
      {arrayPicturesState.map((UrlPicture) => (
        <S.ImgDog key={UrlPicture} onClick={() => getOverlay(UrlPicture)}>
          <Overlay
            urlOverlay={urlOverlay}
            modalIsOpen={modalIsOpen}
            closeModal={closeModal}
          />
          <img src={UrlPicture} alt="Dog" />
        </S.ImgDog>
      ))}
    </S.Grid>
  );
};

export default Dogs;
