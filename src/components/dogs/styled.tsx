import styled from 'styled-components';

export const ImgDog = styled.div`
  img {
    width: 200px;
    height: 200px;
    border: 10px solid black;
    border-radius: 20px;
  }

  img:hover {
    width: 230px;
    height: 230px;
    box-shadow: 4px 4px 4px 2px rgba(0, 0, 0, 0.5);
    cursor: pointer;
  }
`;

export const Grid = styled.div`
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;
