import React from 'react';
import Modal from 'react-modal';

interface Props {
  urlOverlay: string;
  modalIsOpen: boolean;
  closeModal: () => void;
}

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    fontSize: 'large',
    fontFamily: 'cursive',
    fontWeight: 'bold',
  },
};

if (process.env.NODE_ENV !== 'test') {
  Modal.setAppElement('#root');
}

const Overlay: React.FC<Props> = ({urlOverlay, modalIsOpen, closeModal}) => {
  return (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Overlay"
    >
      <img src={urlOverlay} alt="dog overlay" />
      <h4>Pressione ESC para voltar a galeria</h4>
    </Modal>
  );
};

export default Overlay;
